Hi, my name is Heitor Morgado.

Thanks for taking the time to review my work! Here we go:

This is going to be devided between successful tasks and failed attempts.

Successful ones:

So, what I did was a jenkins build (first time using jenkins, yay!) that would clone my repository
https://heitorthedishes@bitbucket.org/heitorthedishes/skip-the-challenge.git everytime a commit was pushed to the master branch.

There's a simple file that gets copied to the container as a html page. No big deal.
The cool part is the docker commands in the build process that get triggered by the BitBucket webhook and push it to my public repository! (screenshots).
Thats the IP address of my ec2 instance running jenkins with the git and bitbucket plugins. I have also added a build console output in the output/ folder.

With the latest image pushed to my public repository, I can refer to AWS ECS to manually deploy it. (screenshots)

i have created a user if you guys want to check out more details of what I did =)

https://heitor-linux.signin.aws.amazon.com/console
user: skip-user
passwd: skipthedishes

Challenges and issues:

I understand the value of teamwork at Skip, which was my first plan (with other three candidates (Fernanda Caroline, Felipe Furlan and Matheus Tavares)), but I had problems building a small docker image for the java webapp the developers were doing, it was like 700MB in size and it was taking ages to be built and pushed to production, so we agreed that I would follow through with the rest of the day with my very own project.
I needed a image with Java and Maven to make their code work, but apparently it's hard to find a small java image with apt (to install maven), or one with both that was less then 700MB.
Thats when I realized i had to do my own thing.

The icing on the cake would be to add one last build shell command line in the jenkins build configuration using the aws ecs command line tool to deploy the new image that was just pushed to the repository in my ECS cluster, but unfortunately I ran out of time and didnt figure out how to do that :( (oh well...)