FROM nginx

COPY html /usr/share/nginx/html/index.html

CMD nginx -g 'daemon off;'